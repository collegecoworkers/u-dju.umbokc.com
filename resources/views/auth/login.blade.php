@extends('layout.app')
@section('content')
<div id="content" class="app-content white bg box-shadow-z2" role="main">
	<div class="app-body" id="view">
		<div class="page-content">
			<div class="row-col">
				<div class="col-lg-9 b-r no-border-md">
					<div class="padding">
						<div class="page-title m-b">
							<h1 class="inline m-a-0">Вход</h1>
						</div>
						<div class="continnesec" >
							<div class="login-content">
								<div class="col-lg-6 col-lg-offset-3">
									<form class="form-login" method="post" action="{{ route('login') }}">
										{{ csrf_field() }}
										<div class="form-group">
											<input class="form-control" name="email" placeholder="Email" type="text" />
											@if ($errors->has('email')) <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span> @endif
										</div>
										<div class="form-group">
											<input class="form-control" name="password" placeholder="Пароль" type="password" />
											@if ($errors->has('password')) <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span> @endif
										</div>
										<button type="submit" class="btn btn-primary">Войти</button>
										<a class="" href="/register">Создать аккаунт</a>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
