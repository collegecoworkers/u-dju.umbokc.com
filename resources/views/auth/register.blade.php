@extends('layout.app')
@section('content')
<div id="content" class="app-content white bg box-shadow-z2" role="main">
	<div class="app-body" id="view">
		<div class="page-content">
			<div class="row-col">
				<div class="col-lg-9 b-r no-border-md">
					<div class="padding">
						<div class="page-title m-b">
							<h1 class="inline m-a-0">Регистрация</h1>
						</div>
						<div class="continnesec" >
							<div class="login-content">
								<div class="col-lg-6 col-lg-offset-3">
									<form class="form-login" method="post" action="{{ route('register') }}">
										{{ csrf_field() }}
										<div class="form-group">
											<input class="form-control" name="full_name" placeholder="Имя" type="text" />
											@if ($errors->has('full_name')) <span class="help-block"><strong>{{ $errors->first('full_name') }}</strong></span> @endif
										</div>
										<div class="form-group">
											<input class="form-control" name="name" placeholder="Логин" type="text" />
											@if ($errors->has('name')) <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span> @endif
										</div>
										<div class="form-group">
											<input class="form-control" name="email" placeholder="Email" type="text" />
											@if ($errors->has('email')) <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span> @endif
										</div>
										<div class="form-group">
											<input class="form-control" name="password" placeholder="Пароль" type="password" />
											@if ($errors->has('password')) <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span> @endif
										</div>
										<div class="form-group">
											<input class="form-control" name="password_confirmation" placeholder="Повторите пароль" type="password" />
											@if ($errors->has('password_confirmation')) <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span> @endif
										</div>
										<button type="submit" class="btn btn-primary">Зарегистрироваться</button>
										<a class="" href="/login">Вход</a>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
