@extends('layout.app')
@section('content')
<div id="content" class="app-content white bg box-shadow-z2" role="main">
	<div class="app-body" id="view">
		<div class="page-content">
			<div class="row-col">
				<div class="col-lg-9 b-r no-border-md">
					<div class="padding">
						<div class="page-title m-b">
							<h1 class="inline m-a-0">{{ $model->title }}</h1>
							<div id="radiobells_container"></div>
							<link href="https://www.radiobells.com/script/style.css" rel="stylesheet" />
							<script>
								var rad_backcolor="#434242";
								var rad_logo = "black";
								var rad_autoplay = !false;
								var rad_width = "responsive";
								var rad_width_px = 330;
								var rad_stations = [[
									'{{ $model->link }}',
									'{{ $model->title }}',
									'{{ $model->alias }}'
								]];
							</script>
							<script src="https://www.radiobells.com/script/v2_1.js" charset="UTF-8"></script>
						</div>
						<div class="jscroll-loading-center">
							<h2>Рекомендации</h2>
							<div class="row">
								@include('item._list')
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
