@extends('layout.app')
@section('content')

<div id="content" class="app-content white bg box-shadow-z2" role="main">
	<div class="app-body" id="view">
		<div class="page-content">
			<div class="row-col">
				<div class="col-lg-9 b-r no-border-md">
					<div class="padding">
						<div class="page-title m-b">
							<h1 class="inline m-a-0">Изменить данные</h1>
						</div>
						<div class="jscroll-loading-center">
							<div class="row">
								<div class="col-lg-12">
									<form method="post" action="/update/{{ $model->id }}" >
										{{ csrf_field() }}
										<div class="form-group">
											<label for="id__title">Название</label>
											<input type="text" class="form-control" id="id__title" name="title" placeholder="Название" required="" value="{{ $model->title }}">
										</div>
										<div class="form-group">
											<label for="id__link">Ссылка</label>
											<input type="text" class="form-control" id="id__link" name="link" placeholder="Ссылка" required="" value="{{ $model->link }}">
										</div>
										<div class="form-group">
											<label for="id__alias">Псевдоним</label>
											<input type="text" class="form-control" id="id__alias" name="alias" placeholder="Псевдоним" required="" value="{{ $model->alias }}">
										</div>
										<button type="submit" class="btn btn-success">Сохранить</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
