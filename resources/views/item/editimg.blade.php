@extends('layout.app')
@section('content')

<div class="">
	<div class="mostcontainer">
		<h2>Изменить изображение</h2>
		<br>
		<img ea-j='w=153x h=102px' src="{{ $model->getImage() }}" alt="">
		<br>
		{!! Form::open(array('url' => '/updateimg/' . $model->id, 'enctype'=>'multipart/form-data')) !!}
			<div class="form-group">
				<label>Изображение</label>
				{!! Form::file('image', ['placeholder' => 'Изображение','class' => 'form-control']) !!}
				@if ($errors->has('image'))<span class="help-block"><strong c#f>{{ $errors->first('image') }}</strong></span>@endif
			</div>
			<button type="submit" class="btn btn-success">Сохранить</button>
		{!! Form::close() !!}
	</div>
</div>


@endsection
