@php
use App\{
	User,
	Favorit
};
@endphp
@foreach ($items as $item)
	@continue(isset($model) and $model->id == $item->id)
	<div class="col-xs-3 col-sm-3 col-md-2">
		<div class="item r">
			<div class="item-media ">
				<a href="/item/{{ $item->id }}" class="item-media-content" style="background-image: url('{{ $item->getImage() }}');"></a>
				<div class="item-overlay center">
					<a href="/item/{{ $item->id }}" class="btn-playpause">Play</a>
				</div>
			</div>
			<div class="item-info">
				@auth
          @if (Favorit::getBy(['item_id' => $item->id, 'user_id' => User::curr()->id ]) == null)
						<div class="item-overlay bottom text-right">
							<a title="Добавить в избранное" href="/to-favorite/{{ $item->id }}" class="btn-favorite"><i class="fa fa-heart-o"></i></a>
						</div>
          @endif
        @endauth
				<div class="item-title text-ellipsis">
					<a href="/item/{{ $item->id }}">{{ $item->title }}</a>
				</div>
			</div>
		</div>
	</div>
@endforeach

