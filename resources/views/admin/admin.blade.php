@extends('layout.app')
@section('content')
<div id="content" class="app-content white bg box-shadow-z2" role="main">
	<div class="app-body" id="view">
		<div class="page-content">
			<div class="row-col">
				<div class="col-lg-9 b-r no-border-md">
					<div class="padding">
						<div class="page-title m-b">
							<h1 class="inline m-a-0">Админ панель</h1>
						</div>
						<div class="jscroll-loading-center">
							<div class="row">
								<div class="col-lg-12">
									<h2>Список пользователей</h2>
									<br>
									<br>
									<table class="table">
										<thead>
											<tr>
												<td>#</td>
												<td>Имя</td>
												<td>Логин</td>
												<td>Почта</td>
												<td>Тип</td>
												<td>Действия</td>
											</tr>
										</thead>
										<tbody>
											@foreach ($users as $item)
											<tr>
												<td>{{$item->id}}</td>
												<td>{{$item->full_name}}</td>
												<td>{{$item->name}}</td>
												<td>{{$item->email}}</td>
												<td>{{$item->getRole()}}</td>
												<td>
													<a c#7 td:n td:u@hov href="/edit-user/{{$item->id}}"><i class="fa fa-edit"></i></a>
													<a c#7 td:n td:u@hov href="/delete-user/{{$item->id}}" onclick="return confirm('Вы уверенны?')"><i class="fa fa-trash"></i></a>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>

								<div class="col-lg-12">
									<h2>Список радио</h2>
									<a href="/add" class="btn btn-primary">Добавить</a>
									<br>
									<br>
									<table class="table">
										<thead>
											<tr>
												<td>#</td>
												<td>Название</td>
												<td>Действия</td>
											</tr>
										</thead>
										<tbody>
											@foreach ($items as $item)
											<tr>
												<td>{{$item->id}}</td>
												<td><a href="/item/{{ $item->id }}">{{$item->title}}</a></td>
												<td>
													<a c#7 td:n td:u@hov href="/edit/{{$item->id}}"><i class="fa fa-edit"></i></a>
													<a c#7 td:n td:u@hov href="/delete/{{$item->id}}" onclick="return confirm('Вы уверенны?')"><i class="fa fa-trash"></i></a>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
