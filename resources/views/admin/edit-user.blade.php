@extends('layout.app')
@section('content')

<div id="content" class="app-content white bg box-shadow-z2" role="main">
	<div class="app-body" id="view">
		<div class="page-content">
			<div class="row-col">
				<div class="col-lg-9 b-r no-border-md">
					<div class="padding">
						<div class="page-title m-b">
							<h1 class="inline m-a-0">Админ панель</h1>
						</div>
						<div class="jscroll-loading-center">
							<div class="row">
								<div class="col-lg-12">
									<h2>Изменить пользователя</h2>
									<br>
									{!! Form::open(array('url' => '/update-user/' .$model->id )) !!}
									<div class="form-group">
										{!! Form::text('full_name', $model->full_name, ['placeholder' => 'Имя', 'class' => 'form-control']) !!}
										@if ($errors->has('full_name')) <span class="help-block"><strong>{{ $errors->first('full_name') }}</strong></span> @endif
									</div>
									<div class="form-group">
										{!! Form::text('name', $model->name, ['placeholder' => 'Логин', 'class' => 'form-control']) !!}
										@if ($errors->has('name')) <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span> @endif
									</div>
									<div class="form-group">
										{!! Form::text('email', $model->email, ['placeholder' => 'Почта', 'class' => 'form-control']) !!}
										@if ($errors->has('email')) <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span> @endif
									</div>
									<div class="form-group">
										<label>Тип</label>
										{!! Form::select('role', ['user' => 'Пользователь', 'admin' => 'Админ'], $model->role, ['class' => 'form-control']) !!}
										@if ($errors->has('is_admin')) <span class="help-block"><strong>{{ $errors->first('is_admin') }}</strong></span> @endif
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-success">Сохранить</button>
									</div>
									{!! Form::close() !!}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
