<!DOCTYPE html>
<html lang="en" ea>
<head>
	<meta charset="utf-8">
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
	<link rel="apple-touch-icon" href="/assets/images/logo.png">
	<meta name="apple-mobile-web-app-title" content="Flatkit">

	<meta name="mobile-web-app-capable" content="yes">
	<link rel="shortcut icon" sizes="196x196" href="/assets/images/logo.png">
	
	<link rel="stylesheet" href="/assets/css/animate.css/animate.min.css" >
	<link rel="stylesheet" href="/assets/css/glyphicons/glyphicons.css" >
	<link rel="stylesheet" href="/assets/css/font-awesome/css/font-awesome.min.css" >
	<link rel="stylesheet" href="/assets/css/material-design-icons/material-design-icons.css" >
	<link rel="stylesheet" href="/assets/css/bootstrap/dist/css/bootstrap.min.css" >

	<link rel="stylesheet" href="/assets/css/styles/app.css" >
	<link rel="stylesheet" href="/assets/css/styles/style.css" >
	<link rel="stylesheet" href="/assets/css/styles/font.css" >
	
	<link rel="stylesheet" href="/assets/libs/owl.carousel/dist/assets/owl.carousel.min.css" >
	<link rel="stylesheet" href="/assets/libs/owl.carousel/dist/assets/owl.theme.css" >
	<link rel="stylesheet" href="/assets/libs/mediaelement/build/mediaelementplayer.min.css" >
	<link rel="stylesheet" href="/assets/libs/mediaelement/build/mep.css" >
	<link rel="stylesheet" href="/assets/css/s.css" >

</head>
<body>

	<div class="app dk" id="app">
		@include('layout.aside')
		@yield('content')
	</div>

</body>
</html>
