@php
	use App\{User};
@endphp
<div id="aside" class="app-aside modal fade nav-dropdown">
	<div class="left navside grey dk" data-layout="column">
		<div class="navbar no-radius">
			<a href="index.html" class="navbar-brand md">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="32" height="32">
					<circle cx="24" cy="24" r="24" fill="rgba(255,255,255,0.2)"/>
					<circle cx="24" cy="24" r="22" fill="#1c202b" class="brand-color"/>
					<circle cx="24" cy="24" r="10" fill="#ffffff"/>
					<circle cx="13" cy="13" r="2"  fill="#ffffff" class="brand-animate"/>
					<path d="M 14 24 L 24 24 L 14 44 Z" fill="#FFFFFF" />
					<circle cx="24" cy="24" r="3" fill="#000000"/>
				</svg>
				<img src="/assets/images/logo.png" alt="." class="hide">
				<span class="hidden-folded inline">{{ config('app.name', 'Laravel') }}</span>
			</a>
		</div>
		<div data-flex class="hide-scroll">
			<nav class="scroll nav-stacked nav-active-primary">
				<ul class="nav" data-ui-nav>
					<li class="nav-header hidden-folded">
						<span class="text-xs text-muted">Сраницы</span>
					</li>
					<li>
						<a href="/">
							<span class="nav-icon">
								<i class="material-icons">
									play_circle_outline
								</i>
							</span>
							<span class="nav-text">Главная</span>
						</a>
					</li>
					 @auth
						<li>
							<a href="/account">
								<span class="nav-icon"><i class="material-icons">grade</i></span>
								<span class="nav-text">Избранное</span>
							</a>
						</li>
						@if (User::isAdmin())
							<li>
								<a href="/admin">
									<span class="nav-icon"><i class="material-icons">assignment_ind</i></span>
									<span class="nav-text">Админ панель</span>
								</a>
							</li>
						@endif
						<li>
							<a href="/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								<span class="nav-icon"><i class="material-icons">exit_to_app</i></span>
								<span class="nav-text">Выйти</span>
							</a>
						</li>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
					@endauth
					@guest
						<li>
							<a href="/login">
								<span class="nav-icon"><i class="material-icons">power_settings_new</i></span>
								<span class="nav-text">Войти</span>
							</a>
						</li>
					@endguest
				</ul>
			</nav>
		</div>
	</div>
</div>
