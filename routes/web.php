<?php

Auth::routes();

Route::get('/', 'SiteController@Index');
Route::get('/item/{id}', 'SiteController@Item');
Route::get('/account', 'SiteController@Account');
Route::get('/all', 'SiteController@All');

Route::get('/to-favorite/{id}', 'ItemController@ToFavorite');
Route::get('/edit/{id}', 'ItemController@Edit');
Route::get('/delete/{id}', 'ItemController@Delete');
Route::post('/create', 'ItemController@Create');
Route::post('/update/{id}', 'ItemController@Update');

Route::get('/items', 'ItemController@Users');
Route::get('/add', 'ItemController@Add');
Route::get('/edit/{id}', 'ItemController@Edit');
Route::get('/delete/{id}', 'ItemController@Delete');
Route::post('/create', 'ItemController@Create');
Route::post('/update/{id}', 'ItemController@Update');


Route::get('/admin', 'AdminController@Admin');

Route::get('/edit-user/{id}', 'AdminController@EditUser');
Route::get('/delete-user/{id}', 'AdminController@DeleteUser');
Route::post('/update-user/{id}', 'AdminController@UpdateUser');


