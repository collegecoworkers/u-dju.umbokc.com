<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class MyModel extends Model{
	protected static $allArr_field = 'title';

	static function allArr(){ return F::toArr(self::all(), static::$allArr_field, 'id'); }
	static function getById($val){ return self::getBy('id', $val); }
	static function getBy($col, $val = null){ if(is_array($col)) return self::queryBy($col)->first(); else return self::queryBy([$col => $val])->first(); }
	static function getsBy($col, $val = null){ if(is_array($col)) return self::queryBy($col)->get(); else return self::queryBy([$col => $val])->get(); }
	static function getCount($col, $val = null){ if(is_array($col)) return self::queryBy($col)->count(); else return self::queryBy([$col => $val])->count(); }
	static function queryBy($arr){ return self::where($arr); }
}
