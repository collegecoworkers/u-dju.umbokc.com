<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Item,
	Favorit,
	User
};

class ItemController extends Controller
{

	function __construct(){
		$this->middleware('auth');
	}


	function Add() {
		$item = new Item();
		return view('item.add')->with([
			'model' => $item,
		]);
	}
	function Edit($id) {
		$item = Item::getBy('id', $id);
		return view('item.edit')->with([
			'model' => $item,
		]);
	}
	function EditImg($id) {
		$item = Item::getById($id);
		return view('item.editimg')->with([
			'model' => $item,
		]);
	}
	function ToFavorite($id) {
		$item = Item::getBy('id', $id);
		$fw = new Favorit;
		$fw->item_id = $item->id;
		$fw->user_id = User::curr()->id;
		$fw->save();
		return redirect()->back();
	}
	function Create(Request $request) {

		$model = new Item();

		$model->title = request()->title;
		$model->link = request()->link;
		$model->alias = request()->alias;

		$model->save();

		return redirect('/admin');
	}
	function Update($id, Request $request) {
		$model = Item::where('id', $id)->first();

		$model->title = request()->title;
		$model->link = request()->link;
		$model->alias = request()->alias;

		$model->save();
		return redirect('/admin');
	}
	function Delete($id) {
		Item::where('id', $id)->delete();
		return redirect()->to('/admin');
	}
}
