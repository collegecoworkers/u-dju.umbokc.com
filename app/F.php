<?php
namespace App;

class F {
	static function toArr($items, $val = 'title', $key = 'id'){
		$items_arr = [];
		foreach ($items as $item){
			if(is_callable($val)){
				$items_arr[$item->{$key}] = $val($item);
			} else {
				$items_arr[$item->{$key}] = $item->{$val};
			}
		}
		return $items_arr;
	}

	static function getField($items, $filed = 'id'){
		$items_fields = [];
		foreach ($items as $item){
			$items_fields[] = is_object($item) ? $item->{$filed} : $item[$filed];
		}
		return $items_fields;
	}

	static function getImage($item){
		if (isset($item->enclosure)) {
			$a = (array) $item->enclosure;
			return @$a['@attributes']['url'];
		}

		if (isset($item->image)) {
			$parser = xml_parser_create();
			xml_parse_into_struct($parser, (string)$item->image, $values);
			foreach ($values as $key => $val) {
				if ($val['tag'] == 'IMG') {
					$first_src = $val['attributes']['SRC'];
					break;
				}
			}

			return $first_src;
		}

		$description = (string) $item->description; //Description
		$html = $description;
		$doc = new \DOMDocument();
		@$doc->loadHTML($html);
		$tags = $doc->getElementsByTagName('img');
		if ($tags != null and $tags[0] != null) {
			return $tags[0]->getAttribute('src');
		}

		if (isset($item->fulltext)) {
			$fulltext = (string) $item->fulltext; //$fulltext
			$html = $fulltext;
			$doc = new \DOMDocument();
			@$doc->loadHTML($html);
			$tags = $doc->getElementsByTagName('img');
			if ($tags != null and $tags[0] != null) {
				return $tags[0]->getAttribute('src');
			}
		}
		return '';
	}

	static function getProp($item, $prop){
		if (isset($item->{$prop})) {
			$val = $item->{$prop};
			if (is_string($val)) {
				return $val;
			} else {
				return $val[0];
			}
		}
		return '';
	}
}
