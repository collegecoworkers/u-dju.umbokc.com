<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Items extends Migration
{
	public function up()
	{
		Schema::create('items', function (Blueprint $table) {
			$table->increments('id');

			$table->string('title');
			$table->string('link');
			$table->string('alias');

			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}

	public function down()
	{
	}
}
