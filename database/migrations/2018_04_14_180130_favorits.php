<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Favorits extends Migration
{
	public function up()
	{
		Schema::create('favorits', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('item_id');
			$table->integer('user_id');

			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}

	public function down()
	{
	}
}
